
public class Machine {

	// prywatna klasa dostepna tylko w nawiasach
	private String name = "Machine Type 1";
	
	public void start() {

		System.out.println("Machine started.");
	}

	public void stop() {

		System.out.println("Machine stopped.");

	}
}
