
public class Car extends Machine {

	// override worse way
	/*
	 * public void start() {
	 * 
	 * System.out.println("Car started."); }
	 */

	@Override
	public void start() {
		System.out.println("Car started.");
	}

	public void wipeWindShield() {

		System.out.println("Wiping windshield.");

	}
	
	
	// nie da sie odniesc do tego bo name jest private
	/* public void showInfo() {
		
		// System.out.println("Car name: " + name);
		
	}
	*/
	
	
	
	
}
